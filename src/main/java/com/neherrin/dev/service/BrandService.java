package com.neherrin.dev.service;

import com.neherrin.dev.entity.BrandEnt;

import java.util.List;

public interface BrandService {

    List<BrandEnt> getAllBrand();

    BrandEnt getBrandById(int id);
}
