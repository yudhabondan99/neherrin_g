package com.neherrin.dev.service.impl;

import com.neherrin.dev.entity.BrandEnt;
import com.neherrin.dev.entity.GraphicEnt;
import com.neherrin.dev.repository.GraphicRepo;
import com.neherrin.dev.service.GraphicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GraphicServiceImpl implements GraphicService {


    @Autowired
    private GraphicRepo graphicRepo;
    @Override
    public List<GraphicEnt> getAllGraphic(){
//        JSONObject josn=new JSONObject();
        return graphicRepo.findAllGraphic();

    }
}
