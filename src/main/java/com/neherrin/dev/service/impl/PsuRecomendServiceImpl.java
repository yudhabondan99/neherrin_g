package com.neherrin.dev.service.impl;

import com.neherrin.dev.constants.DBConstants;
import com.neherrin.dev.db.CouchbaseSDK;
import com.neherrin.dev.entity.PsuRecomendEnt;
import com.neherrin.dev.model.BaseResponse;
import com.neherrin.dev.model.wrapper.PsuRecomendRequest;
import com.neherrin.dev.model.wrapper.PsuRecomendResponse;
import com.neherrin.dev.utility.CustomException;
import org.apache.catalina.authenticator.SavedRequest;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PsuRecomendServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(CouchbaseSDK.class);
    @Transactional
    public static BaseResponse<PsuRecomendResponse> psuRecomend(CouchbaseSDK cbConn, PsuRecomendRequest request) {
        BaseResponse<PsuRecomendResponse> response = new BaseResponse<PsuRecomendResponse>();
        //SaveReceiptReq req = Json.decodeValue(request.toString(), SaveReceiptReq.class);
        String desc;
        String bucketName = "neherrin";
        String transactionDetailId = DBConstants.META_ID_DETAIL + UUID.randomUUID().toString();
        int watt = request.getInt("watt");
        String label = request.getInt("label");
        String memory = request.getString("memory_graphic");
        String name_graphic = request.getString("name_graphic");
        PsuRecomendResponse resp = new PsuRecomendResponse();
        PsuRecomendEnt header = new PsuRecomendEnt();
        JSONObject jsonDetail = new JSONObject("{ }");
        LOGGER.info("request : " + request.toString());
        try {
            //logic when watt dosnt meet mminimum vga
            if(watt >= 500 && label="Gold"|| label="Silver"){
                if(name_graphic = vga RTX 2060 +++){
                    desc="Your Power Supply is Good !!"
                }else{
                    desc="Your Power Supply is over power, you can buy another cheap poswer suply"
                }
            }else if(watt >= 500 && label="White"|| label="Bronze"){
                if(name_graphic = vga RTX 2060 +++){
                    desc="Your Power Supply doesnt meet minimum reqiremnt."
                }else{
                    desc="Your Power Supply is minimum requirment"
                }
            }

            header.setNama(request.getString("nama_psu"));
            header.setDesc(desc);
            header.setLabel(label);
            header.setLabel_value(watt);

            LOGGER.info("inserting neherrin detail...... ");
            String metaId2 = transactionDetailId;
            com.couchbase.client.java.document.json.JsonObject newValue2 = cbConn
                    .insert(bucketName, metaId2, DBConstants.TBL_DETAIL_NEHERRIN,
                            header.toString(), null)
                    .content();
            LOGGER.info(newValue2.toString());


            resp.setStatus("Ok");
            response.setData(resp);
            response.setMessage("success");

        } catch (Exception e) {
            resp.setStatus("Fail");
            response.setMessage("Failed");
            //response.setSuccess(false);
            //response.setMessage(e.getMessage());
            throw new CustomException(cbConn, "Inter Server Error", "PsuRecomService","", e);
        }

        return response;
    }

}
