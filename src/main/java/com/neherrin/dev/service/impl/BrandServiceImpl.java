package com.neherrin.dev.service.impl;

import com.neherrin.dev.entity.BrandEnt;
import com.neherrin.dev.repository.BrandRepo;
import com.neherrin.dev.service.BrandService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

import static javax.management.remote.JMXConnectorFactory.connect;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandRepo brandRepo;
    @Override
    public List<BrandEnt> getAllBrand(){
//        JSONObject josn=new JSONObject();


        return brandRepo.findAllName();

    }


    @Override
    public BrandEnt getBrandById(int id){
        Optional<BrandEnt> brandOpt = brandRepo.findById(id);
        if(brandOpt.isPresent())
            return brandOpt.get();
        else
            throw new RuntimeException("Brand Not Found");
    }

}
