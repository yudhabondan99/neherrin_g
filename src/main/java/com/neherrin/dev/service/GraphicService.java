package com.neherrin.dev.service;

import com.neherrin.dev.entity.BrandEnt;
import com.neherrin.dev.entity.GraphicEnt;

import java.util.List;

public interface GraphicService {

    List<GraphicEnt> getAllGraphic();
}
