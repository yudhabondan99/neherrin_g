package com.neherrin.dev.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "graphic_card")
public class GraphicEnt {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String product_name;

    @Column
    private String gpu_chip;

    @Column
    private String released;

    @Column
    private String bus;

    @Column
    private String memory;

    @Column
    private String gpu_clock;

    @Column
    private String memory_clock;

    @Column
    private String shaders;


    public GraphicEnt() {
    }

    public GraphicEnt(int id, String product_name, String gpu_chip, String released, String bus, String memory, String gpu_clock, String memory_clock, String shaders) {
        this.id = id;
        this.product_name = product_name;
        this.gpu_chip = gpu_chip;
        this.released = released;
        this.bus = bus;
        this.memory = memory;
        this.gpu_clock = gpu_clock;
        this.memory_clock = memory_clock;
        this.shaders = shaders;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getGpu_chip() {
        return gpu_chip;
    }

    public void setGpu_chip(String gpu_chip) {
        this.gpu_chip = gpu_chip;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getGpu_clock() {
        return gpu_clock;
    }

    public void setGpu_clock(String gpu_clock) {
        this.gpu_clock = gpu_clock;
    }

    public String getMemory_clock() {
        return memory_clock;
    }

    public void setMemory_clock(String memory_clock) {
        this.memory_clock = memory_clock;
    }

    public String getShaders() {
        return shaders;
    }

    public void setShaders(String shaders) {
        this.shaders = shaders;
    }


}
