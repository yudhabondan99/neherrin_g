package com.neherrin.dev.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "graphic_card_brand")
public class BrandEnt {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    public BrandEnt() {
    }

    public BrandEnt(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
