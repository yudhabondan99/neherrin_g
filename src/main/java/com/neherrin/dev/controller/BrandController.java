package com.neherrin.dev.controller;

import com.neherrin.dev.entity.BrandEnt;
import com.neherrin.dev.service.BrandService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/neherrin/brand")
//master data brand
public class BrandController {

    @Autowired
    private BrandService brandService;

    @GetMapping
    public List<BrandEnt> getAll(){
        List<BrandEnt> brand = brandService.getAllBrand();
        return brand;
    }
    @GetMapping("/{id}")
    public BrandEnt getById(@PathVariable int id){
        BrandEnt brand = brandService.getBrandById(id);
        return brand;
    }

}
