package com.neherrin.dev.controller;

import com.neherrin.dev.entity.BrandEnt;
import com.neherrin.dev.entity.GraphicEnt;
import com.neherrin.dev.service.BrandService;
import com.neherrin.dev.service.GraphicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/neherrin/graphic")
public class GraphicCardController {
    @Autowired
    private GraphicService graphicService;

    @GetMapping
    public List<GraphicEnt> getAll(){
        var brand = graphicService.getAllGraphic();
        var tes  = "string";
        return brand;
    }
}
