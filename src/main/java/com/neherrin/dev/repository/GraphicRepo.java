package com.neherrin.dev.repository;

import com.neherrin.dev.entity.BrandEnt;
import com.neherrin.dev.entity.GraphicEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GraphicRepo extends JpaRepository<GraphicEnt, String> {

    @Query(value = "SELECT id,product_name,gpu_chip,released,bus,memory," +
            "gpu_clock,memory_clock,shaders FROM graphic_card", nativeQuery = true)
    List<GraphicEnt> findAllGraphic();
}
