package com.neherrin.dev.repository;

import com.neherrin.dev.entity.BrandEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BrandRepo extends JpaRepository<BrandEnt, Integer> {

    @Query(value = "SELECT id,name FROM graphic_card_brand", nativeQuery = true)
    List<BrandEnt> findAllName();
}
