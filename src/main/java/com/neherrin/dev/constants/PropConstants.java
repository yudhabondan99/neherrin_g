package com.neherrin.dev.constants;

public class PropConstants {

    public static final String GLOBAL_DATASOURCE_NAME = "dataSourceName";

    // COUCHBASE DEV CONFIG
    public static final String COUCHBASE_URL_API = "couchbase.apiUrl";
    public static final String COUCHBASE_DB_HOST = "couchbase.databaseHost";
    public static final String COUCHBASE_DB_UN = "couchbase.databaseUsername";
    public static final String COUCHBASE_DB_PW = "couchbase.databasePw";

//    public static final String EMAIL_SVC_URL = "email.svc.url";
//    public static final String EMAIL_SVC_PROCESS_CODE = "email.svc.processCode";
//    public static final String EMAIL_SVC_CHANNEL_ID = "email.svc.channelId";
//
//    public static final String SFTP_HOST = "sftp.host";
//    public static final String SFTP_PORT = "sftp.port";
//    public static final String SFTP_USERNAME = "sftp.username";
//    public static final String SFTP_PW = "sftp.pw";
//    public static final String SFTP_DEFAULT_DIR = "sftp.home.dir";
//    public static final String SFTP_REPORT_DEFAULT_DIR = "sftp.report.home.dir";
//    public static final String SFTP_REPORT_DEFAULT_DIR2 = "sftp.report.home.dir2";
}
