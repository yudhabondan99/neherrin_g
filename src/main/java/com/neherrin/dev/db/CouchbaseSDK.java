package com.neherrin.dev.db;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.error.CASMismatchException;
import com.couchbase.client.java.error.DocumentDoesNotExistException;
import com.couchbase.client.java.query.AsyncN1qlQueryResult;
import com.couchbase.client.java.query.N1qlParams;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.consistency.ScanConsistency;
import com.neherrin.dev.constants.DBConstants;
import com.neherrin.dev.constants.PropConstants;

import rx.Observable;
import rx.Subscriber;

public class CouchbaseSDK {
    private Map<String, String> cbConfig;
    private static final Logger LOGGER = LoggerFactory.getLogger(CouchbaseSDK.class);
    private CouchbaseEnvironment environment = null;
    private Cluster cluster = null;
    private Map<String, Bucket> buckets;
    private N1qlParams n1qlParams = N1qlParams.build().consistency(ScanConsistency.REQUEST_PLUS);

    private long CB_CONNECT_TIMEOUT_SECOND = 3600;
    private long CB_KV_TIMEOUT_SECOND = 3600;
    private long CB_QUERY_TIMEOUT_SECOND = 3600;
    private long CB_EXEC_DATA_TIMEOUT_SECOND = 3600;
    private long CB_SOCKET_CONNECT_TIMEOUT_SECOND = 3600;
    private long CB_OPEN_BUCKET_TIMEOUT_SECOND = 100;
    private Integer CB_KV_END_POINT = 32;
    private Integer CB_REQUEST_BUFFER_SIZE = 32768;
    private Integer MAX_OPEN_BUCKET_RETRY = 3;

    public CouchbaseSDK(Map<String, String> cbConfig) {
        this.cbConfig = cbConfig;
        LOGGER.info(
                "Waiting to connect Couchbase Server " + this.cbConfig.get(PropConstants.COUCHBASE_DB_HOST) + "...");
        try {
            this.environment = DefaultCouchbaseEnvironment.builder()
                    .connectTimeout(TimeUnit.SECONDS.toMillis(CB_CONNECT_TIMEOUT_SECOND))
                    .kvTimeout(TimeUnit.SECONDS.toMillis(CB_KV_TIMEOUT_SECOND))
                    .queryTimeout(TimeUnit.SECONDS.toMillis(CB_QUERY_TIMEOUT_SECOND))
                    .socketConnectTimeout((int) TimeUnit.SECONDS.toMillis(CB_SOCKET_CONNECT_TIMEOUT_SECOND))
                    .build();

            String[] cbHostList = this.cbConfig.get(PropConstants.COUCHBASE_DB_HOST).replaceAll("\\s+", "").split(",");
            this.cluster = CouchbaseCluster.create(this.environment, cbHostList);
            this.cluster.authenticate(this.cbConfig.get(PropConstants.COUCHBASE_DB_UN),
                    this.cbConfig.get(PropConstants.COUCHBASE_DB_PW));
            this.buckets = new HashMap<String, Bucket>();
            LOGGER.info("Couchbase open connection successfully");
        } catch (Exception e) {
            LOGGER.error("Couchbase open connection exception", e);
            throw e;
        }
    }

    public void close() {
        LOGGER.info("Waiting to close connection Couchbase Server " + this.cbConfig.get(PropConstants.COUCHBASE_DB_HOST)
                + "...");
        try {
            for (Map.Entry<String, Bucket> entry : this.buckets.entrySet()) {
                entry.getValue().close();
            }
            if (this.cluster != null) {
                this.cluster.disconnect();
            }
            if (this.environment != null) {
                this.environment.shutdownAsync().toBlocking().single();
            }
            LOGGER.info("Couchbase Server " + this.cbConfig.get(PropConstants.COUCHBASE_DB_HOST) + " success closed");
        } catch (Exception e) {
            LOGGER.error("Couchbase close connection exception", e);
        }
    }

    public JsonArray query(String bucketName, String query, JsonObject param) {
        LOGGER.info("Prepare to execute query select statement : ");
        //LOGGER.info(query);
        if (param != null)
            LOGGER.info("Criterias : " + param.toString());
        try {
            JsonArray rsl = JsonArray.create();
            Bucket bucket = initBucket(bucketName, null);
            N1qlQueryResult n1qlResult = bucket.query(
                    N1qlQuery.parameterized(query, param, n1qlParams),
                    CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
            LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());
            n1qlResult.forEach((row) -> {
                rsl.add(row.value());
            });
            LOGGER.info("Execute successfully.");
            return rsl;
        } catch (Exception e) {
            LOGGER.error("Select data Couchbase exception", e);
            throw e;
        }
    }

    public JsonObject queryN1QLMatrics(String bucketName, String query, JsonObject param) {
        LOGGER.info("Prepare to execute query select statement : ");
        LOGGER.info(query);
        if (param != null)
            LOGGER.info("Criterias : " + param.toString());
        try {
            Bucket bucket = initBucket(bucketName, null);
            N1qlQueryResult n1qlResult = bucket.query(
                    N1qlQuery.parameterized(query, param, n1qlParams),
                    CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
            LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());
            LOGGER.info("Execute successfully.");
            return n1qlResult.info().asJsonObject();
        } catch (Exception e) {
            LOGGER.error("Select data Couchbase exception", e);
            throw e;
        }
    }

    public JsonArray queryAsync(String bucketName, String query, JsonObject param) {
        LOGGER.info("Prepare to execute query async select statement : ");
        //LOGGER.info(query);
        if (param != null)
            LOGGER.info("Criterias : " + param.toString());
        try {
            JsonArray rsl = JsonArray.create();
            Bucket bucket = initBucket(bucketName, null);
            Observable<AsyncN1qlQueryResult> n1qlResult = bucket.async()
                    .query(N1qlQuery.parameterized(query, param, n1qlParams), CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS)
                    .doOnNext(res -> res.info().forEach(t -> LOGGER.info("N1qlMetrics : " + t)));
            List<JsonObject> list = n1qlResult.flatMap(AsyncN1qlQueryResult::rows).map(result -> {
                return result.value();
            }).toList().timeout(50, TimeUnit.SECONDS).toBlocking().single();

            if (!list.isEmpty())
                rsl = JsonArray.from(list);

            LOGGER.info("Execute successfully.");
            return rsl;
        } catch (Exception e) {
            LOGGER.error("Select data Couchbase exception", e);
            throw e;
        }
    }

    public JsonDocument insert(String bucketName, String docId, String tblName, String data, Integer expiry)
            throws Exception {
        LOGGER.info("Prepare to execute insert to couchbase");

        if (docId == null || docId.length() == 0) {
            LOGGER.info("Prepare Generate DocumentID");
            docId = DBConstants.CB_DOCID_KEY + "::" + tblName + "::" + UUID.randomUUID().toString();
            LOGGER.info("Generate DocumentID Successfully");
        }

        JsonObject jsonBody = JsonObject.create();
        jsonBody.put("tbl_name", tblName);
        jsonBody.put("data", JsonObject.fromJson(data));

        LOGGER.info("meta().id : " + docId);
        LOGGER.info("data : " + jsonBody);
        if (expiry != null)
            LOGGER.info("expiry : " + expiry + " seconds");

        try {
            JsonArray rsl = JsonArray.create();
            Bucket bucket = initBucket(bucketName, null);

            JsonDocument document = null;

            if (expiry != null) {
                if (expiry > (30 * 24 * 60 * 60))
                    document = JsonDocument.create(docId, expiry + (int) Instant.now().getEpochSecond(), jsonBody);
            } else {
                document = JsonDocument.create(docId, jsonBody);
            }

            JsonObject param = JsonObject.create();
            param.put("documentId", docId);
            param.put("documentBody", jsonBody);
            String query = "INSERT INTO `" + bucketName + "` (KEY, VALUE) VALUES ($documentId , $documentBody) RETURNING data, tbl_name, meta().id id";

            N1qlQueryResult n1qlResult = bucket.query(N1qlQuery.parameterized(query, param, n1qlParams),
                    CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
            LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());

            LOGGER.info("Insert sync data couchbase successfully");
            return document;
        } catch (Exception e) {
            LOGGER.error("Insert sync data couchbase exception", e);
            throw e;
        }
    }

    public JsonDocument upsert(String bucketName, String docId, String tblName, String data, Integer expiry)
            throws Exception {
        LOGGER.info("Prepare to execute insert to couchbase");

        if (docId == null || docId.length() == 0) {
            LOGGER.info("Prepare Generate DocumentID");
            docId = DBConstants.CB_DOCID_KEY + "::" + tblName + "::" + UUID.randomUUID().toString();
            LOGGER.info("Generate DocumentID Successfully");
        }

        JsonObject jsonBody = JsonObject.create();
        jsonBody.put("tbl_name", tblName);
        jsonBody.put("data", JsonObject.fromJson(data));

        LOGGER.info("meta().id : " + docId);
        LOGGER.info("data : " + jsonBody);
        if (expiry != null)
            LOGGER.info("expiry : " + expiry + " seconds");

        try {
            JsonArray rsl = JsonArray.create();
            Bucket bucket = initBucket(bucketName, null);

            JsonDocument document = null;

            if (expiry != null) {
                if (expiry > (30 * 24 * 60 * 60))
                    document = JsonDocument.create(docId, expiry + (int) Instant.now().getEpochSecond(), jsonBody);
            } else {
                document = JsonDocument.create(docId, jsonBody);
            }

            JsonObject param = JsonObject.create();
            param.put("documentId", docId);
            param.put("documentBody", jsonBody);
            String query = "UPSERT INTO `" + bucketName + "` (KEY, VALUE) VALUES ($documentId , $documentBody) RETURNING data, tbl_name, meta().id id";

            N1qlQueryResult n1qlResult = bucket.query(N1qlQuery.parameterized(query, param, n1qlParams),
                    CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
            LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());

            LOGGER.info("Upsert sync data couchbase successfully");
            return document;
        } catch (Exception e) {
            LOGGER.error("Upsert sync data couchbase exception", e);
            throw e;
        }
    }

    public JsonDocument insertKV(String bucketName, String docId, String tblName, String data, Integer expiry)
            throws Exception {
        LOGGER.info("Prepare to execute insert to couchbase");

        if (docId == null || docId.length() == 0) {
            LOGGER.info("Prepare Generate DocumentID");
            docId = DBConstants.CB_DOCID_KEY + "::" + tblName + "::" + UUID.randomUUID().toString();
            LOGGER.info("Generate DocumentID Successfully");
        }

        JsonObject jsonBody = JsonObject.create();
        jsonBody.put("tbl_name", tblName);
        jsonBody.put("data", JsonObject.fromJson(data));

        LOGGER.info("meta().id : " + docId);
        LOGGER.info("data : " + jsonBody);
        if (expiry != null)
            LOGGER.info("expiry : " + expiry + " seconds");

        try {
            JsonDocument document = null;

            if (expiry != null) {
                if (expiry > (30 * 24 * 60 * 60))
                    document = JsonDocument.create(docId, expiry + (int) Instant.now().getEpochSecond(), jsonBody);
            } else {
                document = JsonDocument.create(docId, jsonBody);
            }
            Bucket bucket = initBucket(bucketName, null);
            bucket.insert(document);
            LOGGER.info("Insert sync data couchbase successfully");
            return document;
        } catch (Exception e) {
            LOGGER.error("Insert sync data couchbase exception", e);
            throw e;
        }
    }

    public CompletableFuture<JsonDocument> insertAsync(String bucketName, String docId, String tblName, String data,
                                                       Integer expiry) throws InterruptedException, ExecutionException {
        LOGGER.info("Prepare to execute insert to couchbase");
        CompletableFuture<JsonDocument> completableFuture = new CompletableFuture<JsonDocument>();

        if (docId == null || docId.length() == 0) {
            LOGGER.info("Prepare Generate DocumentID");
            docId = DBConstants.CB_DOCID_KEY + "::" + tblName + "::" + UUID.randomUUID().toString();
            LOGGER.info("Generate DocumentID Successfully");
        }

        JsonObject jsonBody = JsonObject.create();
        jsonBody.put("tbl_name", tblName);
        jsonBody.put("data", JsonObject.fromJson(data));

        LOGGER.info("meta().id : " + docId);
        LOGGER.info("data : " + jsonBody);
        if (expiry != null)
            LOGGER.info("expiry : " + expiry + " seconds");

        try {
            JsonDocument document = null;

            if (expiry != null) {
                if (expiry > (30 * 24 * 60 * 60))
                    document = JsonDocument.create(docId, expiry + (int) Instant.now().getEpochSecond(), jsonBody);
            } else {
                document = JsonDocument.create(docId, jsonBody);
            }
            final Bucket bucket = initBucket(bucketName, null);
            bucket.async().insert(document).timeout(CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS)
                    .subscribe(new Subscriber<JsonDocument>() {
                        private JsonDocument rsl = null;

                        @Override
                        public void onCompleted() {
                            LOGGER.info("Execute insert async successfully");
                            completableFuture.complete(this.rsl);
                        }

                        @Override
                        public void onError(Throwable e) {
                            LOGGER.error("Insert async to couchbase Exception", e);
                            completableFuture.completeExceptionally(e);
                        }

                        @Override
                        public void onNext(JsonDocument t) {
                            this.rsl = t;
                        }
                    });
        } catch (Exception e) {
            LOGGER.error("Insert async data couchbase exception", e);
            throw e;
        }

        return completableFuture;
    }

    public void updateKV(String bucketName, String docId, String updateDataStr, List<String> removeFields)
            throws Exception {
        LOGGER.info("Prepare updating data");
        LOGGER.info("Document ID : " + docId);
        LOGGER.debug("Data will update : " + updateDataStr);

        /**
         * Construct JsonObject
         */
        final JsonObject objectData = JsonObject.fromJson(updateDataStr);

        /**
         * Get Bucket
         */
        final Bucket bucket = initBucket(bucketName, null);
        while (true) {
            /**
             * Construct JsonDocument
             */
            final JsonDocument current = bucket.get(docId, JsonDocument.class);
            final JsonObject content = current.content();
            content.put("data", objectData.get("data"));
            content.put("tbl_name", objectData.get("tbl_name"));

            // we mutated the content of the document, and the SDK injected the CAS value in
            // there as well
            // so we can use it directly
            try {
                /**
                 * Do Update
                 */
                bucket.replace(current);
                LOGGER.info("Execute update successfully.");
                break; // success! stop the loop
            } catch (CASMismatchException e) {
                // in case a parallel execution already updated the document, continue trying
                LOGGER.info("Check and Set mismatch for item " + docId);
            }
        }
    }

    public JsonArray update(String bucketName, String docId, String updateDataStr, List<String> removeFields)
            throws Exception {
        LOGGER.info("Prepare updating data");
        LOGGER.info("Document ID : " + docId);
        LOGGER.debug("Data will update : " + updateDataStr);

        /**
         * Construct JsonObject
         */
        final JsonObject objectData = JsonObject.fromJson(updateDataStr);

        /**
         * Get Bucket
         */
        final Bucket bucket = initBucket(bucketName, null);

        /**
         * Build Query
         */
        JsonObject param = JsonObject.create();
        param.put("documentId", docId);
        param.put("tblName", objectData.getString("tbl_name"));
        param.put("data", objectData.getObject("data"));
        String query = "UPDATE `" + bucketName + "` USE KEYS $documentId SET tbl_name=$tblName, data=$data RETURNING data, tbl_name, meta().id id";

        /**
         * Execute Query
         */
        try {
            JsonArray rsl = JsonArray.create();
            N1qlQueryResult n1qlResult = bucket.query(N1qlQuery.parameterized(query, param, n1qlParams),
                    CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
            LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());
            n1qlResult.forEach((row) -> {
                rsl.add(row.value());
            });
            LOGGER.info("Execute update successfully.");
            return rsl;
        } catch (Exception e) {
            LOGGER.error("Update data Couchbase exception", e);
            throw e;
        }
    }

    public void deleteKV(String bucketName, String docId) {
        Bucket bucket = initBucket(bucketName, null);
        bucket.remove(docId);
    }

    public JsonArray delete(String bucketName, String docId) {
        JsonArray rslArray = JsonArray.create();

        Bucket bucket = initBucket(bucketName, null);
        JsonObject param = JsonObject.create();
        param.put("key", docId);
        String query = "DELETE FROM `" + bucketName + "` USE KEYS $key returning " + bucketName;

        N1qlQueryResult n1qlResult = bucket.query(N1qlQuery.parameterized(query, param, n1qlParams),
                CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
        LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());

        n1qlResult.forEach((row) -> {
            rslArray.add(row.value());
        });

        return rslArray;
    }

    public Bucket initBucket(String bucketName, Integer counter) {
        Bucket rsl = this.buckets.get(bucketName);
        if (rsl == null) {
            counter = counter == null ? 0 : counter;
            try {
                LOGGER.info("Init bucket " + bucketName);
                rsl = this.cluster.openBucket(bucketName, CB_OPEN_BUCKET_TIMEOUT_SECOND, TimeUnit.SECONDS);
                this.buckets.put(bucketName, rsl);
                LOGGER.info("Init bucket successfully");
            } catch (Exception e) {
                LOGGER.error("Error on open bucket", e);
                if (e instanceof RuntimeException && e.getMessage().contains("TimeoutException")) {
                    if (counter >= MAX_OPEN_BUCKET_RETRY) {
                        throw e;
                    } else {
                        counter = counter + 1;
                        LOGGER.info("Opening bucket failed with TimeoutException on " + counter
                                + " try. Process will retry until " + MAX_OPEN_BUCKET_RETRY + " times");
                        initBucket(bucketName, counter);
                    }
                }
            }
        }
        return rsl;
    }

    public String getCounterKV(String bucketName, String tblName, String fieldName) {
        String rsl = "";
        final String counterName = DBConstants.CB_DOCID_KEY + "::" + tblName + "::COUNTER::" + fieldName;
        final Bucket bucket = initBucket(bucketName, null);
        try {
            rsl = String.valueOf(bucket.counter(counterName, 1).content());
        } catch (DocumentDoesNotExistException e) {
            rsl = String.valueOf(bucket.counter(counterName, 1, 1).content());
        }
        return rsl;
    }


    public String getCounter(String bucketName, String tblName, String fieldName) {
        String rsl = "";
        final String counterName = DBConstants.CB_DOCID_KEY + "::" + tblName + "::COUNTER::" + fieldName;
        final Bucket bucket = initBucket(bucketName, null);
        try {
            String query = "INSERT INTO `" + bucketName + "` VALUES ($key, $value) RETURNING " + bucketName;
            JsonObject param = JsonObject.create();
            param.put("key", counterName);
            param.put("value", 1);
            N1qlQueryResult n1qlResult = bucket.query(N1qlQuery.parameterized(query, param, n1qlParams),
                    CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
            LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());
            JsonArray rslArray = JsonArray.create();
            n1qlResult.forEach((row) -> {
                rslArray.add(row.value());
            });
            if (rslArray.size() > 0) return String.valueOf(rslArray.getObject(0).getInt(bucketName));
            query = "UPDATE `" + bucketName + "` USE KEYS $key SET " + bucketName + " = " + bucketName + " + 1 RETURNING " + bucketName;
            n1qlResult = bucket.query(N1qlQuery.parameterized(query, param, n1qlParams),
                    CB_EXEC_DATA_TIMEOUT_SECOND, TimeUnit.SECONDS);
            LOGGER.info("N1QL Matrics : " + n1qlResult.info().asJsonObject());
            n1qlResult.forEach((row) -> {
                rslArray.add(row.value());
            });
            return String.valueOf(rslArray.getObject(0).getInt(bucketName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rsl;
    }

    public static void main(String[] args) {
        String asd = "123, 321, 111, 222, 333,   444, 111";
        asd = asd.replaceAll("\\s+", "");
        System.out.println(asd);

    }

}
