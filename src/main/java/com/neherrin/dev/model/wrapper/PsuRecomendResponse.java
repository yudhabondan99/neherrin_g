package com.neherrin.dev.model.wrapper;

public class PsuRecomendResponse {

    private String status;
    private String result;
    private String desc;

    public PsuRecomendResponse() {
    }

    public PsuRecomendResponse(String status, String result, String desc) {
        this.status = status;
        this.result = result;
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
