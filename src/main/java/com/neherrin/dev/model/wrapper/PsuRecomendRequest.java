package com.neherrin.dev.model.wrapper;

public class PsuRecomendRequest {
    private String name;
    private String watt;
    private String label;
    private String label_value;

    public PsuRecomendRequest() {
    }

    public PsuRecomendRequest(String name, String watt, String label, String label_value) {
        this.name = name;
        this.watt = watt;
        this.label = label;
        this.label_value = label_value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWatt() {
        return watt;
    }

    public void setWatt(String watt) {
        this.watt = watt;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel_value() {
        return label_value;
    }

    public void setLabel_value(String label_value) {
        this.label_value = label_value;
    }
}
